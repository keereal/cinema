const TIME_S_FOR_ORDER = 60 * 10
const ERROR_TICKETS_AMOUNT = 'You cannot buy more then 4 tickets at once'
const ERROR_TICKETS_BOOKED = 'Sorry this place is booked'
const SUCCESS_TICKETS_BOOKED = 'Your tickets has been booked'

export { TIME_S_FOR_ORDER, ERROR_TICKETS_AMOUNT, ERROR_TICKETS_BOOKED, SUCCESS_TICKETS_BOOKED }
