import {createApp} from "vue";
import {createPinia} from "pinia/dist/pinia";
import App from "./App.vue";

import VueSweetalert2 from "vue-sweetalert2";

import "normalize.css";
import "@/assets/styles/app.scss";
import 'sweetalert2/dist/sweetalert2.min.css';

const app = createApp(App)

app.use(createPinia())
app.use(VueSweetalert2);
app.mount('#app')
