const createTickets = ()=> {
	const types = [
		{name: 'Common', price: 10, rows: 3, places: 10},
		{name: 'Good', price: 15, rows: 1, places: 10},
		{name: 'VIP', price: 20, rows: 1, places: 10},
	];
	const arr = [];

	types.forEach(t => {
		for(let row = 1; row <= t.rows; row++) {
			for(let place = 1; place <= t.places; place++) {
				arr.push({
					id: arr.length + 1,
					sectionName: t.name,
					row,
					place,
					price: t.price,
					reserved: false,
					booked: false,
				});
			}
		}
	});

	return arr;
}

const tickets = createTickets();

export default tickets;
